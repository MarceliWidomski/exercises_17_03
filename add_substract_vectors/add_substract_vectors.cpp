#include <iostream>
using namespace std;

void write(string sentence);
char name();
int dimentions();
void fill(int dimentions, char name, int *pVector);
void writeArray(int dimentions, char name, int *pVector);
void add(int dimentions, int *pVector1, int *pVector2, int *pResult);
void substract(int dimentions, int *pVector1, int *pVector2, int *pResult);

int main() {
	write ("Program adds and substracts 2 vectors which number of dimentions is smaller or equal to 1000");
	int maxSize(1000);
	int dim;
	dim = dimentions();

	char vectorName;
	int vector1[maxSize] = { 0 };
	int *pVector1 = vector1;
	vectorName = name();
	fill(dim, vectorName, pVector1);

	char vectorName2;
	int vector2[maxSize] = { 0 };
	int *pVector2 = vector2;
	vectorName2 = name();
	fill(dim, vectorName2, pVector2);

	char addedName;
	int added[maxSize] = { 0 };
	int *pAdded = added;
	addedName = name();
	add(dim, pVector1, pVector2, pAdded);
	cout << addedName << " = " << vectorName << " + " << vectorName2 << endl;
	writeArray(dim, addedName, pAdded);

	char substractedName;
	int substracted[maxSize] = { 0 };
	int *pSubstracted = substracted;
	substractedName = name();
	substract(dim, pVector1, pVector2, pSubstracted);
	cout << substractedName << " = " << vectorName << " - " << vectorName2
			<< endl;
	writeArray(dim, addedName, pSubstracted);
	return 0;
}
void fill(int dimentions, char name, int *pVector) {
	for (int i = 0; i < dimentions; ++i) {
		cout << "Enter " << i + 1 << " coordinate of vector " << name << ": ";
		cin >> *pVector;
		++pVector;
	}
}
void writeArray(int dimentions, char name, int *pVector) {
	cout << "Vector " << name << " = ";
	for (int i = 0; i < dimentions; ++i) {
		cout << *pVector << " ";
		++pVector;
	}
	cout << endl;
}
void add(int dimentions, int *pVector1, int *pVector2, int *pResult) {
	for (int i = 0; i < dimentions; ++i) {
		*pResult = *pVector1 + *pVector2;
		++pResult;
		++pVector1;
		++pVector2;
	}
}
void substract(int dimentions, int *pVector1, int *pVector2, int *pResult) {
	for (int i = 0; i < dimentions; ++i) {
		*pResult = *pVector1 - *pVector2;
		++pResult;
		++pVector1;
		++pVector2;
	}
}
char name() {
	char name;
	cout << "Enter vector name: " << endl;
	cin >> name;
	return name;
}
int dimentions() {
	int dimentions;
	cout << "Enter number of vector dimentions: ";
	cin >> dimentions;
	return dimentions;
}
void write(string sentence) {
	cout << sentence << endl;
}
