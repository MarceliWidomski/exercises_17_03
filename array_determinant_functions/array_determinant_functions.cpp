
#include <iostream>
using namespace std;

void write(string sentence);
char name();
int size();
void swap(double &number1, double &number2);
void swapRows(int rowNr, int rowNr2, int arraySize, double array[100][100]);
void fill(int dimentions, char name, double array[100][100]);
void writeArray(int dimentions, char name, double array[100][100]);
double determinant(int size, double array[100][100]);

int main() {
	const int maxSize(100);
	double array[maxSize][maxSize] = { 0 };
	write(
			"Program calculates square array determinant by Gaussian Elimination");
	char arrayName;
	arrayName = name();
	int arraySize;
	arraySize = size();
	fill(arraySize, arrayName, array);
	writeArray(arraySize, arrayName, array);
	double det;
	det = determinant(arraySize, array);
	cout << "Det " << arrayName << " = " << det << endl;
	return 0;
}
void write(string sentence) {
	cout << sentence << endl;
}
int size() {
	int dimentions;
	cout << "Enter size of array (e.g. 3 means 3x3 array): ";
	cin >> dimentions;
	return dimentions;
}
char name() {
	char name;
	cout << "Enter array name: " << endl;
	cin >> name;
	return name;
}
void fill(int dimentions, char name, double array[100][100]) { // fills array with elements entered by user
	for (int i = 0; i < dimentions; ++i) {
		for (int j = 0; j < dimentions; ++j) {
			cout << "Enter element with index " << name << " " << i << " " << j
					<< ": ";
			cin >> array[i][j];
		}
		cout << endl;
	}
}
void writeArray(int dimentions, char name, double array[100][100]) { // prints array
	cout << "Array " << name << " :" << endl;
	for (int i = 0; i < dimentions; ++i) {
		for (int j = 0; j < dimentions; ++j) {
			cout << array[i][j] << " ";
		}
		cout << endl;
	}
	cout << endl;
}
double determinant(int size, double array[100][100]) { // calculates array determinant
	for (int i = 0; i < size; ++i) {
		for (int j = i + 1; j <= size; ++j) {
			for (int k = i + 1; k <= size; k++) {
				if (array[i][i] == 0) { // prevents dividing by zero
					int m(i);
					while (array[m][i] == 0) { // seeks for row where element in same column !=0
						++m;
					}
					swapRows(i, m, size, array);
				}
				array[j][k] -= array[i][k] * (array[j][i] / array[i][i]);
			}
		}
	}
	double determinant(1);
	for (int i = 0; i < size; ++i)
		determinant *= array[i][i];
	return determinant;
}
void swap(double &number1, double &number2) { // swaps two numbers
	number1 = number1 - number2;
	number2 = number2 + number1;
	number1 = number2 - number1;
}
void swapRows(int rowNr, int rowNr2, int arraySize, double array[100][100]) { // swaps rows in array
	for (int i = 0; i < arraySize; ++i) {
		swap(array[rowNr][i], array[rowNr2][i]);
	}
}
