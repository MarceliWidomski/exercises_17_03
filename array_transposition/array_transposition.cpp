#include <iostream>
using namespace std;

void write(string sentence);
char name();
void swap(int &number1, int &number2);
int dimention(string dimention = "rows");
void fill(int rows, int columns, char name, int array[100][100]);
void writeArray(int rows, int columns, char name, int array[100][100]);
void transpose(int rows, int columns, int array[100][100],
		int transposedArray[100][100]);

int main() {
	const int maxSize(100);
	int array[maxSize][maxSize] = { 0 };
	write("Program transposes array");
	int arrayName;
	arrayName = name();
	int rowsNr;
	int columnsNr;
	rowsNr = dimention();
	columnsNr = dimention("columns");
	fill(rowsNr, columnsNr, arrayName, array);
	writeArray(rowsNr, columnsNr, arrayName, array);
	write("Transposed array: ");
	int transposedArray[maxSize][maxSize] = { 0 };
	char transposedName;
	transposedName = name();
	transpose(rowsNr, columnsNr, array, transposedArray);
	int transRowsNr(rowsNr);
	int transColumnsNr(columnsNr);
	swap(transRowsNr, transColumnsNr);
	writeArray(transRowsNr, transColumnsNr, transposedName, transposedArray);
	return 0;
}
void write(string sentence) {
	cout << sentence << endl;
}
int dimention(string dimention) { // argument informs user if he enters rows or columns number
	int dimentionSize;
	cout << "Enter number of array " << dimention << ": ";
	cin >> dimentionSize;
	return dimentionSize;
}
void swap(int &number1, int &number2) {
	number1 = number1 - number2;
	number2 = number2 + number1;
	number1 = number2 - number1;
}
char name() {
	char name;
	cout << "Enter array name: " << endl;
	cin >> name;
	return name;
}
void fill(int rows, int columns, char name, int array[100][100]) {
	for (int i = 0; i < rows; ++i) {
		for (int j = 0; j < columns; ++j) {
			cout << "Enter element with index " << name << " " << i << " " << j
					<< ": ";
			cin >> array[i][j];
		}
		cout << endl;
	}
}
void writeArray(int rows, int columns, char name, int array[1000][100]) {
	cout << "Array " << name << ": " << endl;
	for (int i = 0; i < rows; ++i) {
		for (int j = 0; j < columns; ++j) {
			cout << array[i][j] << " ";
		}
		cout << endl;
	}
	cout << endl;
}
void transpose(int rows, int columns, int array[100][100],
		int transposedArray[100][100]) {
	swap(rows, columns);
	for (int i = 0; i < rows; ++i) {
		for (int j = 0; j < columns; ++j) {
			transposedArray[i][j] = array[j][i];
		}
	}
}
