#include <iostream>
using namespace std;

void write(string sentence);
char name();
int dimentions();
void fill(int dimentions, char name, int *pVector);
void writeArray(int dimentions, char name, int *pVector);
void multiplication(int dimentions, double factor, int *pVector1, int *pResult);


int main() {
	write ("Program multiplies vector by a scalar ( number of vector dimentions is smaller or equal to 1000)");
	int maxSize(1000);
	int dim;
	dim = dimentions();

	char vectorName;
	int vector1[maxSize] = { 0 };
	int *pVector1 = vector1;
	vectorName = name();
	fill(dim, vectorName, pVector1);

	int factor;
	write ("Enter factor: ");
	cin >> factor;
	char multipliedName;
	int multiplied[maxSize] = { 0 };
	int *pMultiplied = multiplied;
	multipliedName = name();
	multiplication(dim, factor, pVector1, pMultiplied);
	cout << multipliedName << " = " << vectorName << " * " << factor << endl;
	writeArray(dim, multipliedName, pMultiplied);

	return 0;
}
void fill(int dimentions, char name, int *pVector) {
	for (int i = 0; i < dimentions; ++i) {
		cout << "Enter " << i + 1 << " coordinate of vector " << name << ": ";
		cin >> *pVector;
		++pVector;
	}
}
void writeArray(int dimentions, char name, int *pVector) {
	cout << "Vector " << name << " = ";
	for (int i = 0; i < dimentions; ++i) {
		cout << *pVector << " ";
		++pVector;
	}
	cout << endl;
}
void multiplication(int dimentions, double factor, int *pVector1, int *pResult) {
	for (int i = 0; i < dimentions; ++i) {
		*pResult = *pVector1 * factor;
		++pResult;
		++pVector1;
	}
}
char name() {
	char name;
	cout << "Enter vector name: " << endl;
	cin >> name;
	return name;
}
int dimentions() {
	int dimentions;
	cout << "Enter number of vector dimentions: ";
	cin >> dimentions;
	return dimentions;
}
void write(string sentence) {
	cout << sentence << endl;
}
